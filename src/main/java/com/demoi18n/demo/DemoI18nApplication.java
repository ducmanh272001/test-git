package com.demoi18n.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;

@SpringBootApplication
public class DemoI18nApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoI18nApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String[] language = Locale.getISOLanguages();
		for(String lang : language){
			Locale locale = new Locale(lang);
			System.out.println(lang + " : " +locale.getDisplayLanguage());
		}
	}
}
